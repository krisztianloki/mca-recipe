# mca conda recipe

Home: "https://github.com/epics-modules/mca"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS mca module
